# Configurator

This little script in python push configs from ``configs.json`` file to the api.

Usage of the script :

```text
usage: main.py [-h] [--url URL] token

positional arguments:
  token              Bearer token

optional arguments:
  -h, --help         show this help message and exit
  --url URL, -u URL  url of api
```