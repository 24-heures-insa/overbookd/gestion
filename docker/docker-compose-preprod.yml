version: '3'

networks:
  overbookd_preprod-net-api:
  overbookd_preprod-net-keycloak:
  traefik-public:
    external: true

services:
  db_keycloak:
    container_name: overbookd_preprod_db_keycloak
    image: library/mysql
    restart: always
    command: --default-authentication-plugin=mysql_native_password
    volumes:
      - ./data/preprod/keycloak:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: ${PREPROD_KEYCLOAK_MYSQL_ROOT_PASSWORD}
      MYSQL_DATABASE: ${PREPROD_KEYCLOAK_MYSQL_DATABASE}
      MYSQL_USER: ${PREPROD_KEYCLOAK_MYSQL_USER}
      MYSQL_PASSWORD: ${PREPROD_KEYCLOAK_MYSQL_PASSWORD}
    networks:
      - overbookd_preprod-net-keycloak
    healthcheck:
      test: [ "CMD", "mysqladmin" ,"ping", "-h", "localhost" ]
      timeout: 30s
      retries: 15

  keycloak:
    image: jboss/keycloak
    container_name: overbookd_preprod_keycloak
    depends_on:
      - db_keycloak
    restart: always
    environment:
      DB_VENDOR: MYSQL
      DB_ADDR: db_keycloak
      DB_DATABASE: ${PREPROD_KEYCLOAK_MYSQL_DATABASE}
      DB_USER: ${PREPROD_KEYCLOAK_MYSQL_USER}
      DB_PASSWORD: ${PREPROD_KEYCLOAK_MYSQL_PASSWORD}
      KEYCLOAK_USER: ${PREPROD_KEYCLOAK_USER}
      KEYCLOAK_PASSWORD: ${PREPROD_KEYCLOAK_PASSWORD}
      KEYCLOAK_IMPORT: /tmp/realm-export.json
      KEYCLOAK_FRONTEND_URL: ${PREPROD_KEYCLOAK_FRONTEND_URL}
    volumes:
      - ./assets/json/realm-export.json:/tmp/realm-export.json
    # command:
    #   - "-b 0.0.0.0 -Dkeycloak.import=/tmp/realm-export.json"
    networks:
      - overbookd_preprod-net-keycloak
      - overbookd_preprod-net-api
      - traefik-public
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.preprod-keycloak.entrypoints=websecure"
      - "traefik.http.services.preprod-keycloak.loadbalancer.server.port=8080"
      - "traefik.http.routers.preprod-keycloak.rule=(Host(`preprod.overbookd.${DOMAIN}`) && PathPrefix(`/auth`))"
      - "traefik.http.routers.preprod-keycloak.tls=true"
    healthcheck:
      test: [ "CMD", "curl" ,"--fail", "http://localhost:8080/auth/realms/master" ]
      interval: 60s
      timeout: 3s
      retries: 15
  api:
    container_name: overbookd_preprod_api
    image: registry.gitlab.com/24-heures-insa/overbookd/backend:pre-prod
    restart: always
    networks:
      - overbookd_preprod-net-keycloak
      - overbookd_preprod-net-api
      - traefik-public
    volumes:
      - ./data/preprod/images:/overbookd/backend/images
    environment:
      #      DATABASE_PASSWORD: ${DATABASE_PASSWORD}
      #      DATABASE_HOST: db_project_a
      #      DATABASE_PORT: 3306
      DATABASE_URL: mongodb://${PREPROD_OVERBOOKD_DATABASE_USERNAME}:${PREPROD_OVERBOOKD_DATABASE_PASSWORD}@overbookd_preprod_mongo:27017/
      AUTH_URL: ${PREPROD_OVERBOOKD_AUTH_URL}
      ADMIN_PASSWORD: ${PREPROD_OVERBOOKD_KEYCLOAK_ADMIN_PASSWORD}
      ADMIN_USERNAME: ${PREPROD_OVERBOOKD_KEYCLOAK_ADMIN_USERNAME}
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.preprod-api.entrypoints=websecure"
      - "traefik.http.services.preprod-api.loadbalancer.server.port=2424"
      - "traefik.http.routers.preprod-api.rule=(Host(`preprod.overbookd.${DOMAIN}`) && PathPrefix(`/api`))"
      - "traefik.http.routers.preprod-api.tls=true"
      - "com.centurylinklabs.watchtower.enable=true"

  mongodb:
    container_name: overbookd_preprod_mongo
    image: mongo
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: ${PREPROD_OVERBOOKD_DATABASE_USERNAME}
      MONGO_INITDB_ROOT_PASSWORD: ${PREPROD_OVERBOOKD_DATABASE_PASSWORD}
    networks:
      - overbookd_preprod-net-api
    ports:
    - 27017:27017
    volumes:
      - ./data/preprod/mongo/db:/data/db
      - ./data/preprod/mongo/configdb:/data/configdb

  front:
    container_name: overbookd_preprod_front
    image: registry.gitlab.com/24-heures-insa/overbookd/frontend:pre-prod
    restart: always
    environment:
      NODE_ENV: preprod
    networks:
      - traefik-public
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.preprod-front.entrypoints=websecure"
      - "traefik.http.services.preprod-front.loadbalancer.server.port=1234"
      - "traefik.http.routers.preprod-front.rule=Host(`preprod.overbookd.${DOMAIN}`)"
      - "traefik.http.routers.preprod-front.tls=true"
      - "com.centurylinklabs.watchtower.enable=true"

  # doc:
  #   image: registry.gitlab.com/24-heures-insa/overbookd/documentation
  #   restart: always
  #   networks:
  #     - traefik-public
  #   environment:
  #     SITE: overbookd-doc
  #     WEBSITE_NAME: Documentation
  #     TEMPLATE: classic
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.http.routers.preprod-doc.entrypoints=websecure"
  #     - "traefik.http.services.preprod-doc.loadbalancer.server.port=3001"
  #     - "traefik.http.routers.preprod-doc.rule=Host(`overbookd.${DOMAIN}`) && PathPrefix(`/docs`)"
  #     - "traefik.http.routers.preprod-doc.tls=true"
  #     - "traefik.http.routers.preprod-doc.middlewares=doc-redirect"
  #     - "traefik.http.middlewares.preprod-doc-redirect.redirectregex.regex=^https:\\/\\/([^\\/]+)\\/?$$"
  #     - "traefik.http.middlewares.preprod-doc-redirect.redirectregex.replacement=https://$$1/docs"